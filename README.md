![Logo UNET](unetLogo.png "Logo de la UNET")

"Sistema Control de Estudio"
==========================

### Proyecto I de Programación I (Java)

Primer proyecto de Programación I. Consiste en un sistema de solicitud de constancias estudiantiles programado en lenguaje Java.

Proyecto desarrollado en NetBeans.

#### Desarrolladores
* [Anny Chacón (@AnnyChacon)](https://github.com/AnnyChacon)
* [Gregory Sánchez (@gregsanz182)](https://github.com/gregsanz182)
* [Yeison Fuentes (@Yeisonryhn)](https://github.com/Yeisonryhn)

#### Asignatura
* Nombre: Programación I
* Código: 0416202T
* Profesor: Manuel Sánchez

*Proyecto desarrollado con propositos educativos para la **Universidad Nacional
Experimental del Táchira***
